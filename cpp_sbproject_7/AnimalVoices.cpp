#include <iostream>
using namespace std;

class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Animal Voices:\n" << endl;
	};
};

class Elephant : public Animal
{
public:
	void Voice() 
	{
		std::cout << "Ugh ugh.. Yaaoow!" << endl;
	}
};

class Kitten : public Animal
{
public:
	void Voice() 
	{
		std::cout << "Meow!" << endl;
	}
};

class Frog : public Animal
{
public:
	void Voice()
	{
		std::cout << "Quack-qua" << endl;
	}
};

int main()
{
	const int size = 4;

	Animal* animals[size] = {new Animal, new Elephant, new Kitten, new Frog };

	for (Animal* a : animals)
		a->Voice();
}